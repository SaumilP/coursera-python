"""
Script reads given file and prepares count for mailers
"""
from click._compat import raw_input


def prepare_mailer_count(_filename):
    mailers = dict()
    handle = open(_filename, 'r')
    for line in handle:
        if len(line) < 1 or line == " ":
            continue
        elif line.startswith("From "):
            words = line.split()
            email = words[1].strip()
            mailers[email] = mailers.get(email, 0) + 1
    return mailers


def find_prolific_mailer(mailers):
    prolific_mailer = None
    max_times = 0
    for mailer, times in mailers.items():
        if times > max_times:
            prolific_mailer = mailer
            max_times = times
    print(prolific_mailer, max_times)


if __name__ == "__main__":
    try:
        filename = raw_input("Please enter file-name : ")
        emailers = prepare_mailer_count(filename)
        find_prolific_mailer(emailers)
    except IOError:
        print("Invalid filename provided")
        quit()
