"""
print lines matching given regular expression
"""
import re
from pip._vendor.distlib.compat import raw_input

if __name__ == '__main__':
    try:
        filename = raw_input("Please enter a filename : ")
        handler = open(filename, "r")
        for line in handler:
            line = line.rstrip()
            if re.search('^From:', line):
                print(line)
    except IOError:
        print("Invalid filename")
        quit()
