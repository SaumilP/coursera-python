"""
print lines matching given regular expression
"""
import re

if __name__ == '__main__':
    s = 'Hellow from csev@umich.edu to cwen@iupui.edu about the meeting @2PM'
    lst = re.findall('\S+@\S+', s)
    print(lst)
    quit()
