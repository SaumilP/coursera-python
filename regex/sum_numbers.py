import re

from pip._vendor.distlib.compat import raw_input

__author__ = "saumil patel"


def sum(_filename):
    with open(_filename, mode='r', buffering=1) as in_file:
        contents = in_file.read()
        numbers = re.findall("[0-9]+", contents.replace('\n', " "))
        res = sum(int(s) for s in numbers)
    return res


if __name__ == "__main__":
    filename = raw_input("Please enter filename: ")
    if len(filename) < 1:
        filename = "regex_sum_346647.txt"
    print("Sum =>", sum(filename))
