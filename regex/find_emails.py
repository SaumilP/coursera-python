"""
print lines matching given regular expression
"""
import re
from pip._vendor.distlib.compat import raw_input

if __name__ == '__main__':
    try:
        handler = open("mbox-short.txt", "r")
        for line in handler:
            line = line.rstrip()
            #x = re.findall('\S+@\S+', line)
            x = re.findall('[a-zA-Z0-9]\S*@\S*[a-zA-Z0-9]+', line)
            if len(x) > 0:
                print(x)
    except IOError:
        print("Invalid filename")
        quit()
