"""
Calculate hourly rate with condition
"""
from click._compat import raw_input


def computepay(hours, rate):
    _hours = int(hours)
    _rate = float(rate)
    if hours <= 40:
        return _hours * _rate
    else:
        return (40*_rate) + (_hours-40)*(1.5*_rate)

if __name__ == '__main__':
    hrs = raw_input("Please enter hours : ")
    rt = raw_input("Please enter rate/hour: ")
    try:
        print(computepay(hrs, rt))
    except ValueError:
        print("Failed to calculate hourly rate")
        quit()
