"""
Program grades given score
"""


def grade(given_score):
    _score = float(given_score)
    if 0.0 >= _score >= 1.0:
        raise Exception("Provided score is out-of-range")
    else:
        if _score >= 0.9:
            return "A"
        elif _score >= 0.8:
            return "B"
        elif _score >= 0.7:
            return "C"
        elif _score >= 0.6:
            return "D"
        else:
            return "F"

if __name__ == '__main__':
    score = input("Please enter score: ")
    try:
        print(grade(score))
    except ValueError:
        print("Invalid value provided")
        quit()
