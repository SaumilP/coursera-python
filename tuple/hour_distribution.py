"""
Searchs for email sending user and determines hour distribution
"""
from click._compat import raw_input


def print_hour_distribution(_filename):
    hour_dict = dict()
    handler = open(_filename)
    for line in handler:
        if not line.startswith("From "):
            continue
        else:
            words = line.strip().split()
            formatted_date = words[5]
            hour = formatted_date.split(":")[0]
            hour_dict[hour] = hour_dict.get(hour, 0) + 1
    for hour, no_of_times in sorted(hour_dict.items()):
        print(hour, no_of_times)


if __name__ == '__main__':
    try:
        filename = raw_input("Please enter a filename: ")
        if len(filename) < 1 or filename == "": filename = "mbox-short.txt"
        print_hour_distribution(filename)
    except IOError:
        print("Invalid filename provided")
        quit()
