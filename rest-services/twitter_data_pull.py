#!/usr/bin/python

import urllib
import twurl
import json

TWITTER_URL = 'https://api.twitter.com/1.1/friends/list.json'

while True:
    print ''
    acct = raw_input('Enter Twitter Account:')
    if(len(acct)<1):
        break

    url = twurl.augment(TWITTER_URL,{'screen_name': acct, 'count': '5'} )
    #print 'Retrieving', url
    connection = urllib.urlopen(url)

    data = connection.read()
    headers = connection.info().dict
    #print 'Remaining', headers['x-rate-limit-remaining']
    js = json.loads(data)
    #print json.dumps(js, indent=4)
    for users in js['users'] :
        following = users['screen_name']
        status = (users['status']['text'])[:50] if 'status' in users else None
        print 'Following: {0} [ status:{1} ]'.format(following, status)
