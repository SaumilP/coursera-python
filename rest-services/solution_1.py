#!/usr/bin/python

import urllib
import json

serviceurl = 'http://python-data.dr-chuck.net/geojson?'

address = raw_input('Enter location : ')
if len(address) < 1:
    raise ValueError

url = serviceurl + urllib.urlencode({'sensor':'false', 'address': address})
print 'Retrieving', url
uhandler = urllib.urlopen(url)
data = uhandler.read()
print 'Retrieved',len(data),'characters'

try: jsondata = json.loads(str(data))
except: jsondata = None
if 'status' not in jsondata or jsondata['status'] != 'OK':
    print '==== Failure To Retrieve ===='
    print data
else:
    print 'Place id',jsondata["results"][0]["place_id"]
