#!/usr/bin/python

import urllib
import json

__doc__ = "Script looping parser uses given json data and extracts values"
__author__ = "saumil patel"

url = raw_input('Please enter a valid url : ')
if len(url) < 1:
    raise ValueError

print 'Retrieving', url
uhandler = urllib.urlopen(url)
data = uhandler.read()
print 'Retrieved',len(data),'characters'

try: jsondata = json.loads(data)
except: jsondata = None

if jsondata is None or len(jsondata) == 0:
    print "Failed to retrieve data"
else:
    comments = jsondata["comments"]
    print 'Count:',len(comments)
    print 'Sum:', sum([int(c["count"]) for c in comments])
