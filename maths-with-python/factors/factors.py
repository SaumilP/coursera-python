"""
Find the factors of an integer
"""


def factors(positiveNumber):
    for idx in range(1, positiveNumber + 1):
        if positiveNumber % idx == 0:
            print(idx)


if __name__ == '__main__':
    number = input("Please enter a positive number: ")
    number = float(number)
    if number > 0 and number.is_integer():
        factors(int(number))
    else:
        print("Please enter a positive integer")
