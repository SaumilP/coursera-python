"""
Script reads given file and prints From Email addresses
"""
from click._compat import raw_input


def printContent(_filename):
    handle = open(_filename, 'r')
    for line in handle:
        if line.startswith("From:"):
            print(line.rstrip())


if __name__ == "__main__":
    filename = raw_input("Please enter file-name : ")
    printContent(filename)
    quit()
