"""
Reads file content and searches for specific content
"""
from click._compat import raw_input


count = None
total = None


def searchcontent(_filename, content_to_search="X-DSPAM-Confidence:"):
    handle = open(_filename, 'r')
    for line in handle:
        if line.strip().startswith(content_to_search):
            global count
            count = 1 if count is None else count + 1
            pos = line.find(":")
            floatVal = float(line[pos+1:].strip())
            global total
            total = floatVal if total is None else (total+floatVal)


if __name__ == "__main__":
    try:
        filename = raw_input("Please enter filename: ")
        searchcontent(filename)
        print("Average spam confidence:", total/count)
    except(ValueError, IOError) as e:
        print("Failed to read and search file content")
