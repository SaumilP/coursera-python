"""
Script reads given file and prints content of the file
"""
from click._compat import raw_input


def printContent(_filename):
    handle = open(_filename, 'r')
    text = handle.read()
    print(text.upper())


if __name__ == "__main__":
    filename = raw_input("Please enter file-name : ")
    printContent(filename)
    quit()
