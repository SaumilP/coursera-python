"""
Find smallest & largest number
"""
from click._compat import raw_input

minimum = None
maximum = None


def smallest(num):
    global minimum
    if minimum is None or minimum > num:
        minimum = num


def largest(num):
    global maximum
    if maximum is None or maximum < num:
        maximum = num


if __name__ == "__main__":
    while True:
        try:
            num = raw_input("Please enter a number: ")
            if num == "done":
                print("Minimum is ", minimum)
                print("Maximum is ", maximum)
                break
            smallest(int(num))
            largest(int(num))
        except ValueError:
            print("Invalid input")
