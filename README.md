*** Python course ***
This repository contains programmes used in Coursera tought Python courses.
Most of the exercises are based on mentioned books in these courses.

*** Requirements ***
Basic requirements include:
* a decent spec'd laptop
* python 3.2.5
* some knowledge on executing python scripts
