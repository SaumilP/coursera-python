#!/usr/bin/python

__doc__ = "This script parses webpage and list video source"
__author__ = "saumil patel"

import urllib
from BeautifulSoup import *

url = raw_input('Please enter a valid url : ')
html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)

# retrieve all of the anchor tags
tags = soup('video')
for tag in tags:
    print tag.get('src', None)
