#!/usr/bin/python

__doc__ = "This script counts number of words from web url"
__author__ = "saumil patel"

import urllib

counts = dict()
fhand = urllib.urlopen('http://www.py4inf.com/code/romeo.txt')
for line in fhand:
    words = line.split()
    for word in words:
        counts[word] = counts.get(word, 0) + 1
print counts
