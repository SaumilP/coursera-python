#!/usr/bin/python

import urllib
from BeautifulSoup import *
from collections import OrderedDict

url = raw_input('Enter URL: ')
count = raw_input('Enter count: ')
pos = raw_input('Enter position: ')

names = OrderedDict()
idx = 0
while idx < int(count):
    print "Retrieving:", url
    html = urllib.urlopen(url).read()
    soup = BeautifulSoup(html)

    # retrieve all of the anchor tags
    tag = soup('a')[int(pos)-1]
    url = tag.get('href', None)
    name = tag.contents[0]
    names[name] = url
    idx += 1

print "Name:", names.keys()[-1]
