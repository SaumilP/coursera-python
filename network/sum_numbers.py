#!/usr/bin/python

import urllib
from BeautifulSoup import *

__doc__ = "This script downloads content from the webpage, extracts numbers and sums them up"
__author__ = "saumil patel"

url = raw_input('Please enter a valid url : ')
if len(url) < 1:
    url = "http://python-data.dr-chuck.net/comments_42.html"
html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)

# retrieve all of the spans tags with class='comments'
spans = soup('span', attrs={'class':'comments'})
numbers = list()
cnt = 0
for spans in spans:
    content = spans.string
    if content.isdigit():
        numbers.append(int(content))
    cnt += 1

print "Count", cnt
print "Sum", sum(numbers)
