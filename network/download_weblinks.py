#!/usr/bin/python

import math
import httplib2
import urllib
from BeautifulSoup import BeautifulSoup, SoupStrainer
from urlparse import urlparse

def size(_size):
   if (_size == 0): return '0B'
   size_name = ("KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(_size,1024)))
   p = math.pow(1024,i)
   s = round(_size/p,2)
   return '%s %s' % (s,size_name[i-1])

def get(url):
    """
    Downloads file into default downloads directory
    """
    file_name = url.split('/')[-1]
    u = urllib.urlopen(url)
    f = open(file_name, 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "(info) Downloading: %s [ %s ]" % (file_name, size(file_size))
    file_size_dl = 0
    block_sz = 8192*4
    while True:
        buffer = u.read(block_sz)
        if not buffer: break
        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,
    f.close()

def prepare_url(uri_path, url):
    """
    Uses given uri part and given url to prepare absolute web-resource file
    """
    filename = uri_path[uri_path.find("/")+1:]
    parts = urlparse(url)
    return "{0}://{1}/{2}".format(parts.scheme,parts.netloc,uri_path)

def download(url, links):
    """
    Downloads web-resource links from links item list
    """
    for link in links:
        weblink = link if link.startswith('http://') else prepare_url(link, url)
        get(weblink)

def scrap(url, extension):
    """
    scraps webpage for anchor tag and downloads webresources
    """
    http = httplib2.Http()
    status, response = http.request(url)
    links = list()
    for link in BeautifulSoup(response, parseOnlyThese=SoupStrainer("a")):
        href = link['href']
        if href.endswith(extension):
            if not href.startswith('http'): href = prepare_url(href, url)
            if href not in links: links.append(href)
    return download(url, links)

if __name__ == "__main__":
    url = raw_input('Website Url : ')
    extension = raw_input("Extension: ")
    if len(url) < 1 or len(extension) < 1: raise ValueError("No url/extension provided")
    scrap(url, extension)
