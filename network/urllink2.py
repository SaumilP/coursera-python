#!/usr/bin/python

__doc__ = "This script parses webpage response from given web url"
__author__ = "saumil patel"

import urllib
from BeautifulSoup import *

url = raw_input('Enter - ')
html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)

# retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
    print 'TAG:', tag
    print 'URL:', tag.get('href', None)
    print 'Content:', tag.contents[0]
    print 'Attrs:', tag.attrs
