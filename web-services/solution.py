#!/usr/bin/python

import urllib
import xml.etree.ElementTree as ET

__doc__ = "Script downloads content from given weburl and counts number of characters and sums of numbers"
__author__ = "saumil patel"

url = raw_input("Please enter a valid weburl: ")
if len(url) < 1 : raise ValueError("Invalid weburl given")

print 'Retrieving', url
urlhandler = urllib.urlopen(url)
data = urlhandler.read()
print 'Retrieved',len(data),'characters'

tree = ET.fromstring(data)
comments = tree.findall('comments/comment')
print 'Count:',len(comments)
print 'Sum:', sum([int(c.find('count').text) for c in comments])
