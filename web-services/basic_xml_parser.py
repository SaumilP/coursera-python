#!/usr/bin/python

__doc__ = "Script parsing given XML data and extract values"
__author__ = "saumil patel"

import xml.etree.ElementTree as ET

data = '''
<person>
    <name>Saumil</name>
    <phone type="int">+27 11 1234567</phone>
    <email hide="yes" />
</person>'''

tree = ET.fromstring(data)
print "Name:", tree.find('name').text
print "Attr:", tree.find('email').get('hide')
