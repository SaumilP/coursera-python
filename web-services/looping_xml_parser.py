#!/usr/bin/python

__doc__ = "Script looping parser uses given XML data and extracts values"
__author__ = "saumil patel"

import xml.etree.ElementTree as ET

data = '''
<stuff>
    <users>
        <user x="2">
            <id>001</id>
            <name>John</name>
        </user>
        <user x="7">
            <id>009</id>
            <name>Jane</name>
        </user>
    </users>
</stuff>'''

tree = ET.fromstring(data)
users = tree.findall('users/user')
print "User Count:", len(users)
for user in users:
    print 'Name:', user.find('name').text
    print 'Id:', user.find('id').text
    print 'Attrib:', user.get('x')
