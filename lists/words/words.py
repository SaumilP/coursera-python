"""
Read file and split into words
"""
from click._compat import raw_input


if __name__ == "__main__":
    try:
        filename = raw_input("Please enter a filename: ")
        handler = open(filename, "r")
        allwords = list()
        for line in handler:
            words = line.rstrip().split()
            for word in words:
                if not any(word in s for s in allwords):
                    allwords.append(word)
        allwords.sort()
        print(allwords)
    except IOError:
        print("Failed to find file")
        quit()
