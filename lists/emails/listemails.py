"""
Lists email addresses
"""
from click._compat import raw_input

if __name__ == '__main__':
    try:
        emails = list()
        filename = raw_input("Please enter filename: ")
        if len(filename) < 1 : filename = "mbox-short.txt"
        handler = open(filename)
        for line in handler:
            if line.startswith("From "):
                words = line.split()
                email = words[1].strip()
                print(email)
                emails.append(email)
        print("There were", len(emails), "lines in the file with From as the first word")
    except IOError:
        print("Invalid filename provided")
        quit()
